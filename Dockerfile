FROM tiangolo/uvicorn-gunicorn-fastapi:python3.11

COPY ./requirements.txt /app/requirements.txt
COPY ./users.txt /app/users.txt

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./api /app/app
