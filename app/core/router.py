from typing import Annotated
from fastapi import APIRouter, Depends, Body
from sqlalchemy.cyextension.util import Mapping
from sqlalchemy.orm import Session
from app.database import get_db, use_db
from uuid import UUID
from . import models, schemas, dependencies

router = APIRouter()


# CRUD Operations
@router.post("/", response_model=schemas.UserInDB)
def create_user(item_data: Annotated[schemas.User, Body()], db: Session = Depends(get_db)):
    new_user = models.User(**item_data.dict())
    db.add(new_user)
    db.commit()
    return new_user


@router.get("/{user_id}", response_model=schemas.UserInDB)
def get_user_by_id(user: Mapping = Depends(dependencies.valid_user_id)):
    return user


@router.put("/{user_id}", response_model=schemas.UserInDB)
def update_user_by_id(new_data: schemas.User, user: Mapping = Depends(dependencies.valid_user_id),
                      db: Session = Depends(get_db)):
    for k, v in vars(new_data).items():
        setattr(user, k, v) if v else None
    db.commit()
    return user


@router.get("/", response_model=list[schemas.UserInDB])
def get_all_users(db: Session = Depends(get_db)):
    users = db.query(models.User).all()
    return users


@router.get("/duplicates")
def get_duplicates(db: Session = Depends(get_db)):
    pass
