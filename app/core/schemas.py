from enum import Enum, StrEnum, auto
from typing import Optional

from pydantic import BaseModel, Field, UUID4


class Country(StrEnum):
    POLAND = "Poland"
    BARLAND = "Barland"
    FOOLAND = "Fooland"


class User(BaseModel):
    name: str
    surname: str
    country: Optional[Country] = Field(default=None)


class UserInDB(User):
    id: UUID4

    class Config:
        from_attributes = True
