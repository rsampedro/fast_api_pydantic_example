import uuid

from sqlalchemy import Column, String, Text, UUID

from app.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(
        "id", UUID, default=lambda: uuid.uuid4(), primary_key=True
    )

    name = Column(String, nullable=False)
    surname = Column(String, nullable=False)
    country = Column(String, nullable=True)
