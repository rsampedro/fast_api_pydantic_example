# Here would go the services that retrive things from DB, usually the shape of the Database is hidden from the router
# / API itself, so all the queries would be defined in services, here
from uuid import UUID

from app.core import models
from app.database import get_db


def get_user_by_id(user_id: UUID) -> "models.User":
    session = next(get_db())
    return session.query(models.User).filter_by(id=user_id).first()
