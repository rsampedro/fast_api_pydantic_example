# Here go all the dependencies -> All the functions that check constraints of DB to allow or not allow requests e.g
# get_db, even though it's in database.py would perfectly eb
from typing import Mapping

from pydantic import UUID4

from app.core import services


async def valid_user_id(user_id: UUID4) -> Mapping:
    post = services.get_user_by_id(user_id)
    if not post:
        raise Exception()

    return post
