from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base

from .config import SQLALCHEMY_DATABASE_URL

engine = create_engine(SQLALCHEMY_DATABASE_URL)

SessionLocal = sessionmaker(autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

def use_db(func):
    def wrapper():
        try:
            db = SessionLocal()
            func()
        finally:
            db.close()
    return wrapper

