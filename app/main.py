import csv
from http import HTTPStatus

from fastapi import FastAPI

from .config import RECREATE_DATA
from .core import models
from app.core.router import router
from .database import engine, get_db

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
app.include_router(router, prefix="/users")

if RECREATE_DATA:
    db = next(get_db())
    db.query(models.User).delete()
    db.commit()
    with open("users.txt") as csv_file:
        reader = csv.reader(csv_file, delimiter=",")
        for row in reader:
            user = models.User(
                name=row[0].strip(),
                surname=row[1].strip(),
                country=row[2].strip() if row[2].strip() != "" else None,
            )
            db.add(user)
        db.commit()


@app.get("/", status_code=HTTPStatus.OK)
async def root():
    return "OK"
