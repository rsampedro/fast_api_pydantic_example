DOCKER_IMG_NAME=fastapi_pydantic_example-api

build:
	docker-compose build api

run: build
	docker-compose start $(DOCKER_IMG_NAME)

test: venv
	venv/bin/python -m pytest tests

black: venv
	venv/bin/black api tests main.py

venv:
	@python -m venv venv
	venv/bin/pip install --upgrade pip
	venv/bin/pip install -r requirements-dev.txt

clean-venv:
	rm -rf venv
	rm app.db
	rm -rf .pytest_cache
	rm -rf __pycache__

clean-docker:
	docker image rm $(DOCKER_IMG_NAME)

clean: clean-venv clean-docker