# APP ENDPOINTS

- CRUD operations on model
    - [x] Create (POST)
    - [x] Read (GET)
    - Update
        - [x] Full Update (PUT)
        - [ ] Partial Update (PATCH)
    - [ ] Delete (DELETE)
- [ ] Filter database
- [ ] JSON-like queries to db
- [ ] Async
- [ ] Websockets

# DATABASE

## GENERAL

- Database Relations
    - [ ] 1-1
    - [ ] 1-Many
    - [ ] Many-Many
- [ ] Reference binaries
- [ ] Integrate Alembic

## REDIS

- Dunno

# TESTS

- UNIT
    - [ ] Test function
    - [ ] Test function w Mocks
- INTEGRATION
    - [ ] Test CRUD ENDPOINTS
        - [ ] TEST POST
        - [ ] TEST GET
        - [ ] TEST PUT
        - [ ] TEST DELETE

# CI-CD

- Makefile
    - [ ] Install
    - [ ] Run tests
    - [ ] Create / Teardown service
    - 
- [ ] App Dockerfile
- Docker-compose
    - [ ] App
    - [ ] PostgreSQL
    - [ ] Redis
    - [ ] RabbitMQ
    -
- [ ] Kubernetes shit

# DOCS

- [ ] Swagger Api